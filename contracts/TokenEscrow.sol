pragma solidity >=0.5.3 <0.6.0;

import 'openzeppelin-solidity/contracts/ownership/Ownable.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

/**
 * @title ERC20Basic
 * @dev Simpler version of ERC20 interface
 * @dev see https://github.com/ethereum/EIPs/issues/179
 */
contract ERC20Basic {
    function totalSupply() public view returns (uint256);
    function balanceOf(address who) public view returns (uint256);
    function transfer(address to, uint256 value) public returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
}


contract ERC20 is ERC20Basic {
    function allowance(address owner, address spender) public view returns (uint256);
    function transferFrom(address from, address to, uint256 value) public returns (bool);
    function approve(address spender, uint256 value) public returns (bool);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}

/**
 * @title Token Distribution Escrow contract
 * @author Manan Patel (manan77patel@gmail.com)
 * @dev 
 */
contract TokenEscrow is Ownable {


    event TokenEscrowCreated(address indexed owner, address indexed tokenContract);
    event TokenClaimSuccessful(address indexed sender, uint indexed value);
    event TokenClaimFailed(address indexed sender, uint indexed value);
    event TokenClaimRemoved(bytes32 indexed claim);
    event MultipleClaimsRemoved(bytes32[] indexed claim);

 
    using SafeMath for uint256;
    mapping(bytes32 => uint) private tokenClaims;
    uint private outstandingClaimsTotal;
    address tokenContract;

    /**
     * @dev TokenEscrow constructor
     * @param _owner refers to the owner of the contract
     */
    constructor(
        address _owner,
        address _tokenContract
    )
        public
        Ownable()
    {
        transferOwnership(_owner);
        tokenContract = _tokenContract;
        emit TokenEscrowCreated(_owner, tokenContract);
    }

    /**
    * verify for enough allowance
    */
    modifier checkAllowance(uint _amount){
        ERC20 erc20token = ERC20(tokenContract);
        uint amount = erc20token.allowance(owner(), address(this));
        require(amount >= outstandingClaimsTotal.add(_amount));
        _;
    }

    /**
     * @dev retrieve the address & token balance of token holders (each time retrieve partial from the list)
     * @param _claim claim id
     * @param _to destination address
     * @return array of accounts and array of balances
     */
    function claimTokens(
       bytes32 _claim,
        address _to
    )
    external
    returns (bool)
    {
        bytes32 _claimId = keccak256(abi.encodePacked(_claim));
        //if there is no claim for sender, then revert
        require(tokenClaims[_claimId] > 0, "Invalid Claim");
        ERC20 erc20token = ERC20(tokenContract);
        //transfer claimed tokens to sender
        bool success = erc20token.transferFrom(owner(), _to, tokenClaims[_claimId]);
        //if successfully claimed, then remove it
        if(success) {
            emit TokenClaimSuccessful(msg.sender, tokenClaims[_claimId]);
            outstandingClaimsTotal.sub(tokenClaims[_claimId]);
            tokenClaims[_claimId] = 0;
            
        }
        else {
            emit TokenClaimFailed(msg.sender, tokenClaims[_claimId]);
        }
        
    }

    /**
     * @dev add a new claim to the token claims list
     * @param _claim claim id
     * @param _value amount of claim
     */
    function addClaim(
        bytes32 _claim,
        uint _value
    )
    external
    checkAllowance(_value)
    onlyOwner
    {
        bytes32 _claimId = keccak256(abi.encodePacked(_claim));
        tokenClaims[_claimId] = _value;
        //increment total claims amount
        outstandingClaimsTotal.add(_value);
    }
    
   /**
     * @dev add multiple claims to token claims list
     * @param _claims array of claim ids
     * @param _values array of values for each claim
     */
    function addMultipleClaims(
        bytes32[] calldata _claims,
        uint[] calldata _values
    )
    external 
    onlyOwner
    {
        for (uint8 i; i < _claims.length; i++) {
            bytes32 _claimId = keccak256(abi.encodePacked(_claims[i]));
            tokenClaims[_claimId] = _values[i];
            //increment total claims amount
            outstandingClaimsTotal.add(_values[i]);
        }
    
    }

    /**
     * @dev removes existing claim from token claims list
     * @param _claim claim id
     */
    function removeClaim(
        bytes32 _claim
    )
    external
    onlyOwner
    {
        bytes32 _claimId = keccak256(abi.encodePacked(_claim));
        outstandingClaimsTotal.sub(tokenClaims[_claimId]);
        tokenClaims[_claimId] = 0;
        emit TokenClaimRemoved(_claim);
    }

    /**
     * @dev removes multiple claims to token claims list
     * @param _claims array of claim ids to be remove
     */
    function removeMultipleClaims(
        bytes32[] calldata _claims
    )
    external 
    onlyOwner
    {
        for (uint8 i; i < _claims.length; i++) {
            bytes32 _claimId = keccak256(abi.encodePacked(_claims[i]));
            outstandingClaimsTotal.sub(tokenClaims[_claimId]);
            tokenClaims[_claimId] = 0;
        }
    
        emit MultipleClaimsRemoved(_claims);
    }
    
   
    /**
     * @dev get length of claims
     */
    function getOutstandingClaims()
        external
        view
        onlyOwner
        returns (uint)
    {
        return outstandingClaimsTotal;
    }

    /**
     * @dev kill the contract
     */
    function kill()
        external
        onlyOwner
    {
        selfdestruct(address(uint160(owner())));
    }

    /**
     * @dev fallback function prevents ether transfer to this contract
     */
    function()
        external
        payable
    {
        revert('Invalid ether transfer');
    }

    //TODO : expiry of claims
    //TODO : remove all claims
    //TODO : erc20 honey pot
}